using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Linq;

namespace csharpback_1
{
    public class TodoRepository : ITodoRepository
    {
        private ConcurrentDictionary<int, TodoDto> dicTodo;

        public TodoRepository()
        {
            dicTodo = new ConcurrentDictionary<int, TodoDto>();
        }

        public TodoDto AddTodo(TodoDto todo)
        {
            if (dicTodo.Count() == 0)
            {
                dicTodo.TryAdd(1, todo);
            }
            else
            {

                int maxKey = dicTodo.Max(k => k.Key);
                dicTodo.TryAdd(maxKey + 1, todo);
            }
            return todo;
        }

        public TodoDto GetDto(int todoId)
        {
            return dicTodo.GetValueOrDefault(todoId);
        }

        public IEnumerable<TodoDto> GetAllTodo(){
            return dicTodo.Values;
        }

    }
}