using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.Concurrent;
namespace csharpback_1
{
    public interface ITodoRepository
    {
        TodoDto AddTodo(TodoDto todo);

        TodoDto GetDto(int todoid);

        IEnumerable<TodoDto> GetAllTodo();

    }
}