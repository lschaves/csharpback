﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;

namespace csharpback_1.Controllers
{
    [ApiController]
    [Route("todo")]
    public class TodoController : ControllerBase
    {

        private readonly ILogger<TodoController> _logger;
        private ITodoRepository _repository;
        public TodoController(ILogger<TodoController> logger, ITodoRepository repository)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpGet("{id}")]
        public TodoDto GetTodo(int id)
        {

            _logger.LogInformation("Get recebido");
            return _repository.GetDto(id);
        }

        [HttpGet]
        public IEnumerable<TodoDto> GetTodoList(){
            return _repository.GetAllTodo();
        }

        [HttpPost("new")]
        public ActionResult<TodoDto> PostTodo(TodoDto todo)
        {

            _repository.AddTodo(todo);

            return todo;
        }

    }
}
